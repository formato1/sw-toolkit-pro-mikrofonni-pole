#include "config.h"
#include "buffer.h"
#include "network.h"
#include "parser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int parse_config(int argc , char ** argv, struct config * config_out){
    if(config_out == NULL) {
        fprintf(stderr, "NULL POINTER ERROR! config_out is NULL!");
        return -1;
    }

    if(argc != 5){
		fprintf(stderr, "Cannot run without arguments. Arguments must be <sample_rate> <TDM_frames_to_buffer> <audio_device> <number_of_channels>\n");
		return -2;
	}

    config_out->number_of_channels = atoi(argv[4]);
    config_out->audio_device       = argv[3];           //something like hw:0 or plughw:0 is expected
    config_out->buffer_frames      = atoi(argv[2]);
    config_out->sample_rate        = atoi(argv[1]);

    if( config_out->buffer_frames      == 0 || 
        config_out->sample_rate        == 0 || 
        config_out->number_of_channels == 0 ){

		fprintf(stdout, "Arguments are invalid. Arguments bust be <sample_rate> <TDM_frames_to_buffer> <audio_device> <number_of_channels>\n");

		return -3;
	}

    fprintf(stdout, "number of channels: %d\n", config_out->number_of_channels);
    fprintf(stdout, "sample rate: %d buffer frames: = %d\n", config_out->sample_rate, config_out->buffer_frames);

    config_out->buffer_frames *= config_out->number_of_channels / 2;
    config_out->sample_rate   *= config_out->number_of_channels / 2;

    fprintf(stdout, "sr:%d bf:%d\n", config_out->sample_rate, config_out->buffer_frames);

    /*todo*/
    config_out->output_file_name = "rawdata.b";
    config_out->mode = OFFLINE_MODE;

    return 0;
}   

int parse_config_from_message(struct message * msg, struct config * config_out, struct buffer * string_mem){
    // "<sample_rate> <TDM_frames_to_buffer> <audio_device> <number_of_channels> <output_file_name/immediate mode>\n"
    printf("\n");
    if(config_out == NULL) {
        fprintf(stderr, "NULL POINTER ERROR! config_out is NULL!");
        return -1;
    }
    
    char * msg_string = msg->text.memory;
    int res;
    
    res = get_int_value(msg_string, &msg_string, (int *) &(config_out->sample_rate));
    if(res < 0) return -1;
    res = get_int_value(msg_string, &msg_string, &(config_out->buffer_frames));
    if(res < 0) return -2;
    res = get_string_value(string_mem, msg_string, &msg_string, &(config_out->audio_device));
    if(res < 0) return -3;
    res = get_int_value(msg_string, &msg_string, &(config_out->number_of_channels));
    if(res < 0) return -4;
    res = get_string_value(string_mem, msg_string, &msg_string, &(config_out->output_file_name));
    if(res < 0) return -5;

    fprintf(stdout, "number of channels: %d\n", config_out->number_of_channels);
    fprintf(stdout, "sample rate: %d buffer frames: = %d\n", config_out->sample_rate, config_out->buffer_frames);

    config_out->buffer_frames *= config_out->number_of_channels / 2;
    config_out->sample_rate   *= config_out->number_of_channels / 2;

    fprintf(stdout, "sr:%d bf:%d\n", config_out->sample_rate, config_out->buffer_frames);

    config_out->mode = ONLINE_MODE; 

    if(strcmp(config_out->output_file_name, "IMMEDIATE") == 0){
        config_out->mode = ONLINE_IMMEDIATE_MODE;
    }

    return 0;   
}