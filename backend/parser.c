#include "parser.h"
#include <stdlib.h>
#include <stdio.h>


int get_string_value(struct buffer * string_mem, char * start, char ** end, char ** value){
    if(start == NULL){
        fprintf(stderr, "ERROR: get_string_value - Parameter const char * start is NULL.\n");
        *value = NULL;
        *end = NULL;
        return -1;
    }

    char * src = start;
    char * dest = string_mem->memory + string_mem->dat_size;
    *value = dest;

    while(*src == VALUE_SEPARATOR) ++src;

    while(*src != VALUE_SEPARATOR && 
          *src != MESSAGE_SEPARATOR && 
          string_mem->dat_size < (string_mem->mem_size - 1)){ /* mem_size - 1 because after loop 
                                                                must be '\0' stored to string_mem*/

        *dest = *src;
        string_mem->dat_size += 1;
        ++dest;
        ++src;
    }

    //deal with separator
    if(*src == MESSAGE_SEPARATOR)
        src = NULL; //end of message, nothing to be processed
    else
        ++src; //pass value separator


    *dest = '\0'; //null terminate the string
    string_mem->dat_size += 1; //adjust data size
    *end = src; //save where the copying ended

    return 0;
}

int get_int_value(const char * start, char ** end, int * value){
    if(start == NULL){
        fprintf(stderr, "ERROR: get_int_value - Parameter const char * start is NULL.\n");
        *value = 0; 
        return -1;
    }

    *value = strtol(start, end, 10);

    if(start == *end){
        fprintf(stderr, "ERROR: get_int_value - No integer value in string.\n");
        return -2;
    }
    ++(*end);
    return 0;
}