#pragma once
#include "config.h"
#include "utils.h"
#include "buffer.h"

#include <stdbool.h>
#include <alsa/asoundlib.h>

/**
 * @brief Sets-up pcm interface.
 * 
 * @param audio_buffer buffer to be allocated and initialise according to the configurations
 * @param properties setu-up configurations 
 * @param pcm_handle ALSA interface handle
 * @param already_open indicating if pcm interface was already accessed
 * @return error code
 */
int setup_pcm_interface(struct buffer * audio_buffer, struct config * properties, snd_pcm_t ** pcm_handle, bool already_open);
/**
 * @brief Reads buffer of data from pcm interface.
 * 
 * @param pcm_buffer buffer to be filled
 * @param buffer_frames number of frames to be read
 * @param capture_handle ALSA interface handle
 * @return int 
 */
int read_pcm(char * pcm_buffer, int buffer_frames, snd_pcm_t *capture_handle);