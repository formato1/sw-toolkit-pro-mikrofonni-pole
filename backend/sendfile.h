#pragma once
#include "buffer.h"
#include "message.h"

/**
 * @brief Sends file to client.
 * 
 * @param client_sock client socket
 * @param filename name of file to be sent
 * @param network_buffer network buffer to be used
 * @return error code
 */
int send_file(int client_sock, const char * filename, struct buffer * network_buffer);