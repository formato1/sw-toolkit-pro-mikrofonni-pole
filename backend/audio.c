#include "audio.h"
#include "buffer.h"


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>

#define M_DBG 0

int setup_pcm_interface(struct buffer * audio_buffer, struct config * properties, snd_pcm_t ** pcm_handle, bool already_open){
							 
    snd_pcm_hw_params_t *hw_params;
    snd_pcm_format_t format = SND_PCM_FORMAT_S32_LE;
    snd_pcm_t *capture_handle;
    int err;

    if(already_open == true){
        if((err = snd_pcm_close(*pcm_handle))< 0){
            fprintf(stderr, "error occured while closing device (%s)\n",
                    snd_strerror(err));
            return -1;
        }

        free_buffer(audio_buffer);
    }
    if ((err = snd_pcm_open(&capture_handle, properties->audio_device, SND_PCM_STREAM_CAPTURE, 0)) < 0) {
        fprintf(stderr, "cannot open audio device %s (%s)\n",
                properties->audio_device,
                snd_strerror(err));
        return -2;
    }

    fprintf(stdout, "audio interface %s opened\n", properties->audio_device);

    if ((err = snd_pcm_hw_params_malloc(&hw_params)) < 0) {
        fprintf(stderr, "cannot allocate hardware parameter structure (%s)\n",
                snd_strerror(err));
        return -3;
    }

    if(M_DBG) fprintf(stdout, "hw_params allocated\n");

    if ((err = snd_pcm_hw_params_any(capture_handle, hw_params)) < 0) {
        fprintf(stderr, "cannot initialize hardware parameter structure (%s)\n",
                snd_strerror(err));
        return -4;
    }

    if(M_DBG) fprintf(stdout, "hw_params initialized\n");

    if ((err = snd_pcm_hw_params_set_access(capture_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
        fprintf(stderr, "cannot set access type (%s)\n",
                snd_strerror(err));
        return -5;
    }

    if(M_DBG) fprintf(stdout, "hw_params access setted\n");

    if ((err = snd_pcm_hw_params_set_format(capture_handle, hw_params, format)) < 0) {
        fprintf(stderr, "cannot set sample format (%s)\n",
                snd_strerror(err));
        return -6;
    }

    if(M_DBG) fprintf(stdout, "hw_params format setted\n");

    if ((err = snd_pcm_hw_params_set_rate_near(capture_handle, hw_params, &(properties->sample_rate), 0)) < 0) {
        fprintf(stderr, "cannot set sample rate (%s)\n",
                snd_strerror(err));
        return -7;
    }

    if(M_DBG) fprintf(stdout, "hw_params rate setted\n");

    if ((err = snd_pcm_hw_params_set_channels(capture_handle, hw_params, 2)) < 0) {
        fprintf(stderr, "cannot set channel count (%s)\n",
                snd_strerror(err));
        return -8;
    }

    if(M_DBG) fprintf(stdout, "hw_params channels setted\n");

    if ((err = snd_pcm_hw_params(capture_handle, hw_params)) < 0) {
        fprintf(stderr, "cannot set parameters (%s)\n",
                snd_strerror(err));
        return -9;
    }

    if(M_DBG) fprintf(stdout, "hw_params setted\n");

    snd_pcm_hw_params_free(hw_params);

    if(M_DBG) fprintf(stdout, "hw_params freed\n");

    if ((err = snd_pcm_prepare(capture_handle)) < 0) {
        fprintf(stderr, "cannot prepare audio interface for use (%s)\n",
                snd_strerror(err));
        return -10;
    }

    fprintf(stdout, "audio interface prepared\n");
    
    *pcm_handle = capture_handle; 
    init_buffer(audio_buffer, properties->buffer_frames * 2 * snd_pcm_format_width(format) / 8);
    
    return 0;
}

int read_pcm(char * pcm_buffer, int buffer_frames, snd_pcm_t *capture_handle){
	int err;
	err = snd_pcm_readi(capture_handle, pcm_buffer, buffer_frames);
	if(err < 0){
		fprintf(stderr, "read from audio interface failed %d(%s)\n", err, snd_strerror(err));

		err = snd_pcm_prepare(capture_handle);
		if (err < 0){
			printf("recovery failed\n");
			return -1;
		}
		printf("stream recovered\n");
    }

    return 0;
}
