#pragma once
#define OFFLINE_MODE 1
#define ONLINE_MODE  2
#define ONLINE_IMMEDIATE_MODE 3

#include "buffer.h"
#include "message.h"

struct config
{
    unsigned int sample_rate;
    int buffer_frames;
    int number_of_channels;
    char * audio_device;
    char * output_file_name;
    int mode;
};

/**
 * @brief Parses config parameters. Otimised for program parameters
 * 
 * @param argc 
 * @param argv 
 * @param config_out 
 * @return error code
 */
int parse_config(int argc, char ** argv, struct config * config_out);

/**
 * @brief Parses config parameters. Otimised for program parameters
 * 
 * @param msg received message
 * @param config_out output
 * @param string_mem memory to store strings, should not be directly used by programmer
 * @return error code
 */
int parse_config_from_message(struct message * msg, struct config * config_out, struct buffer * string_mem);