#pragma once

#include<stdio.h>
#include<stdlib.h>


void make_console_nonblocking();
void make_console_blocking();
void make_console_immediate();

int open_output_file(char * file_name, FILE ** file_descriptor);