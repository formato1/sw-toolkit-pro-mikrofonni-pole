#pragma once

/**
 * @brief general buffer structure 
 * @param memory allocated memory
 * @param mem_size size of allocated memory
 * @param dat_size number of relevant bytes
 * @param last_pos position of last processed byte, indicates beginning of unprocessed part
 */
struct buffer{
    char * memory;
    int mem_size;
    int dat_size;
    int last_processed;
};

/**
 * @brief Allocates and initialises buffer.
 * 
 * @param ptr buffer structure to be initialised
 * @param size size to be allocated
 * @return error code
 */
int init_buffer(struct buffer * ptr, int size);
/**
 * @brief frees buffer
 * 
 * @param ptr buffer to be freed
 */
void free_buffer(struct buffer * ptr);
/**
 * @brief resets buffer
 * 
 * @param ptr buffer to be reset
 */
void reset_buffer(struct buffer * ptr);
