#include "message.h"
#include "utils.h"
#include <string.h>
#include <stdlib.h>

void add_to_message(struct message * msg, struct buffer * network_buffer){
    int max_size_to_write = network_buffer->dat_size - (network_buffer->last_processed + 1);

    if(((msg->text.dat_size + max_size_to_write) > msg->text.mem_size)||
        (msg->complete == true)){
			//message is complete or received data would overflow message buffer.
            reset_message(msg);
	}

    int i = network_buffer->last_processed + 1;
    //fill message buffer with data from network buffer
    for(; i < network_buffer->dat_size; ++i){
        if(network_buffer->memory[i] == '\n'){
            //mesage is complete
            msg->complete = true;
            //null terminate received string
            msg->text.memory[msg->text.dat_size] = '\0';
            //update last processed
            network_buffer->last_processed = i;
            return;
        }
        
        msg->text.memory[msg->text.dat_size++] = network_buffer->memory[i];
    }
    //update last processed
    network_buffer->last_processed = i;


}


int string_to_buffer(const char * string, struct buffer * network_buffer){
    int remaining_size = network_buffer->mem_size - (network_buffer->last_processed + 1); 
    char * buf_ptr = network_buffer->memory + network_buffer->last_processed + 1;
    int msg_size = strlen(string) + 1; //plus null terminator

    if(msg_size > remaining_size){
         //not enough memory size in buffer
         return -1;
    }

    strncpy(buf_ptr, string, remaining_size);

    network_buffer->dat_size += msg_size;
    return 0;
}

int init_message(struct message * msg, int size){
    int result = init_buffer(&(msg->text), size);

    return result;
}

void reset_message(struct message * msg){
    msg->complete = false;
    reset_buffer(&(msg->text));
}

void free_message(struct message * msg){
    free_buffer(&(msg->text));
}