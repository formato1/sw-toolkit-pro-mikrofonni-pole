#pragma once
#include "buffer.h"

#define VALUE_SEPARATOR ' '
#define MESSAGE_SEPARATOR '\0'

/**
 * @brief Parses next string value from incoming buffer. To be used with parameter list messages.
 * 
 * @param string_mem memory for storing strings
 * @param start poiter to the first character to be processed
 * @param end poiter to last character processed, first to be processed in next call
 * @param value pointer to string value
 * @return error code
 */
int get_string_value(struct buffer * string_mem, char * start, char ** end, char ** value);

/**
 * @brief Parses next int value from incoming buffer. To be used with parameter list messages.
 * 
 * @param start poiter to the first character to be processed
 * @param end poiter to last character processed, first to be processed in next call
 * @param value pointer to int value
 * @return int 
 */
int get_int_value(const char * start, char ** end, int * value);
