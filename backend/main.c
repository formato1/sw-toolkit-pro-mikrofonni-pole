#include "audio.h"
#include "config.h"
#include "network.h"
#include "utils.h"
#include "sendfile.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define PORT 5555
#define BUFF_SIZE 1024
#define MSG_SIZE 64
#define STRING_MEM_SIZE 256

#define LOOP_ACCEPT_NEXT_CLIENT 1
#define LOOP_EXIT_PROGRAM 2

struct config properties;
struct buffer audio_buffer;
snd_pcm_t * pcm_handle;
FILE * data_file_out;
bool already_open;

int listening_sock, client_sock;
struct sockaddr_in client_address;
struct message msg_in;
struct buffer network_buff_in;
struct buffer network_buff_out;

struct buffer string_mem;

int loop(){
    if(accept_TCP_client(listening_sock, &client_sock, &client_address) < 0) return LOOP_ACCEPT_NEXT_CLIENT;

    printf("Client connected.\n");
    printf("Waiting to receive configuration message.\n");

    //receive config message
    receive_message(client_sock, &network_buff_in, &msg_in);
    int res = parse_config_from_message(&msg_in, &properties, &string_mem);

    if(res < 0){
        printf("Parsing config message failed.\n");
        string_to_buffer("Bad format of configuration message.\n", &network_buff_out);
        send_buffer(client_sock, &network_buff_out); 
        return LOOP_ACCEPT_NEXT_CLIENT;
    }

    if(setup_pcm_interface(&audio_buffer, &properties, &pcm_handle, already_open) < 0){ 
        string_to_buffer("Audio interface setup failed.\n", &network_buff_out);
        send_buffer(client_sock, &network_buff_out);    
        return LOOP_ACCEPT_NEXT_CLIENT;
    }

    printf("\n");

    already_open = true;

    bool known_message;

    while(1){
        known_message = false;
        res = receive_message(client_sock, &network_buff_in, &msg_in);
        if(res < 0) return LOOP_ACCEPT_NEXT_CLIENT;

        if(strcmp(msg_in.text.memory, "record") == 0){
            known_message = true;
            printf("recording\n");
            
            //if immediate mode, make sure there is no valid file descriptor
            if(properties.mode == ONLINE_IMMEDIATE_MODE){
                data_file_out = NULL;
            }//fetch mode, open file to store the data
            else if(open_output_file(properties.output_file_name, &data_file_out) < 0){
                string_to_buffer("Output file can not be open.\n", &network_buff_out);
                send_buffer(client_sock, &network_buff_out);    
                return LOOP_EXIT_PROGRAM;
            }
            //reset message, because receive_partial_message_nonblock, does not reset message on read unlika receive_message
            reset_message(&msg_in);

            while(1){
                //if message is received end
                receive_partial_message_nonblock(client_sock, &network_buff_in, &msg_in);
                if(msg_in.complete == true){
                    //no matter what message has been received
                    //TODO
                    printf("%s\n", msg_in.text.memory);
                    break;
                }

                //read data form audio device
                if(read_pcm(audio_buffer.memory, properties.buffer_frames, pcm_handle) < 0) return LOOP_EXIT_PROGRAM;

                if(properties.mode == ONLINE_IMMEDIATE_MODE){
                    //immediate mode sends buffer after read
                    //remember after send_buffer data from buffer are deleted
                    audio_buffer.dat_size = audio_buffer.mem_size;

                    if(send_buffer(client_sock, &audio_buffer) < 0) 
                        return LOOP_ACCEPT_NEXT_CLIENT;
                }
                else{
                    //there must be else, because when upper if is true, no data un audio_buffer remain
                    fwrite(audio_buffer.memory, sizeof(char), audio_buffer.mem_size, data_file_out);
                }
            }

            if(properties.mode != ONLINE_IMMEDIATE_MODE)
                fclose(data_file_out);

        }

        if(strcmp(msg_in.text.memory, "fetch") == 0){
            known_message = true;
            //receve file name
            receive_message(client_sock, &network_buff_in, &msg_in); 
            printf("File sending started\n");
            send_file(client_sock, msg_in.text.memory ,&network_buff_out);
            printf("File sending completed\n");
            //return LOOP_EXIT_PROGRAM;
        }

        if(strcmp(msg_in.text.memory, "disconnect") == 0){
            return LOOP_ACCEPT_NEXT_CLIENT;
        }

        if(strcmp(msg_in.text.memory, "exit") == 0){
            return LOOP_EXIT_PROGRAM;
        }

        if(known_message == false){
            printf("unknown message: %s\n", msg_in.text.memory);
            string_to_buffer("unknown message\n", &network_buff_out);
            send_buffer(client_sock, &network_buff_out);
        }
    }

    return LOOP_ACCEPT_NEXT_CLIENT;
}

int main(int argc, char ** argv){
    printf("______________\n\n");   
    printf("I2S TDM SERVER\n");
    printf("______________\n");                                                                              
    already_open = false;

    if(setup_TCP_server(&listening_sock, PORT) < 0) exit(2);

    init_buffer(&network_buff_in, BUFF_SIZE);
    init_buffer(&network_buff_out, BUFF_SIZE);
    init_buffer(&string_mem, STRING_MEM_SIZE);
    init_message(&msg_in, MSG_SIZE);
    
    int res = 0;
    do{
        printf("\nlistening on port %d ...\n\n", PORT);
        res = loop();
        printf("\nclient disconnected\n");
    }while(res == LOOP_ACCEPT_NEXT_CLIENT);

    free_buffer(&audio_buffer);
    free_buffer(&network_buff_in);
    free_buffer(&network_buff_out);
    free_buffer(&string_mem);
    free_message(&msg_in);

    return 0;
}