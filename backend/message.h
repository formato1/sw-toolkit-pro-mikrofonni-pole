#pragma once

#include "buffer.h"
#include <stdbool.h>

struct message{
    struct buffer text;
    bool complete;
};

/**
 * @brief Extends message with data from network buffer.
 * 
 * @param msg message to be extended
 * @param network_buffer received buffer
 */
void add_to_message(struct message * msg, struct buffer * network_buffer);
/**
 * @brief Adds string to network buffer.
 * 
 * @param string string to be added
 * @param network_buffer buffer to be filled
 * @return error code
 */
int string_to_buffer(const char * string, struct buffer * network_buffer);
/**
 * @brief Frees message buffer.
 * 
 * @param msg message to be freed
 */
void free_message(struct message * msg);
/**
 * @brief Initialises message buffer.
 * 
 * @param msg message to be initialised
 * @param size size of message buffer
 * @return error code
 */
int init_message(struct message * msg, int size);
/**
 * @brief Resets message buffer.
 * 
 * @param msg message to be reset
 */
void reset_message(struct message * msg);
