#include "sendfile.h"
#include "network.h"

#include <stdio.h>

int send_file(int client_sock, const char * filename, struct buffer * network_buffer){
    FILE * f = fopen(filename, "rb");
    reset_buffer(network_buffer);
    int result;
    if(f == NULL) return -1;

    do{
        result = fread(network_buffer->memory, 1, network_buffer->mem_size, f);
        network_buffer->dat_size = result;
        send_buffer(client_sock, network_buffer);
        
    }while(result == network_buffer->mem_size);

    fclose(f);

    return 0;
}