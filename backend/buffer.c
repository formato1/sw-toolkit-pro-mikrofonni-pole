#include "buffer.h"

#include<stdlib.h>
#include<string.h>

int init_buffer(struct buffer * ptr, int size){
    ptr->last_processed = -1;
    ptr->dat_size = 0;
    ptr->mem_size = size;
    ptr->memory = NULL;
    ptr->memory = malloc(size);
    if(ptr->memory == NULL) return -1;
    memset(ptr->memory, 0, size);
    return 0;
}

void free_buffer(struct buffer * ptr){
    free(ptr->memory);
}

void reset_buffer(struct buffer * ptr){
    ptr->last_processed = -1;
    ptr->dat_size = 0;
    memset(ptr->memory, 0, ptr->mem_size);
}