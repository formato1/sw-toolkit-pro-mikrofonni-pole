#include"utils.h"
#include<stdio.h>
#include<stdlib.h>
#include<termios.h>
#include<errno.h>
#include<string.h>
#include<unistd.h>
#include<fcntl.h> 

void make_console_nonblocking(){
    int flags = fcntl(STDIN_FILENO , F_GETFL, 0);
    fcntl(STDIN_FILENO , F_SETFL, flags | O_NONBLOCK);
}


void make_console_blocking(){
    int flags = fcntl(STDIN_FILENO , F_GETFL, 0);
    fcntl(STDIN_FILENO , F_SETFL, flags & (~O_NONBLOCK));
}

void make_console_immediate(){
    struct termios oldt, newt;
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON);          
    tcsetattr( STDIN_FILENO, TCSANOW, &newt);
}

int open_output_file(char * file_name, FILE ** file_descriptor){
    *file_descriptor = fopen(file_name, "wb");
    if(*file_descriptor == NULL){
        fprintf(stderr, "Error occured while opening file %s. %s %d\n", file_name, strerror(errno), errno);
        return -1;
    }
    return 0;
}
