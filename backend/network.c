#include "network.h"
#include "buffer.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>


int setup_TCP_server(int *out_listening_sock, int port){
	int sock;
	int res;
	struct sockaddr_in local_addr;
	
	//create socket
	sock = socket(AF_INET, SOCK_STREAM, 0);

	if(sock < 0){
		fprintf(stderr, "Creating socket failed! (%s)\n", strerror(errno));
		return -1;
	}
	
	//When program fails to close socket before exit, port reamins allocated. 
	//This option allows to reuse already allocated port.
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR,&(int){1}, sizeof(int));
	
	//setup address
	memset(&local_addr, 0, sizeof(local_addr));
	local_addr.sin_family = AF_INET;
	local_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	local_addr.sin_port = htons(port);
	
	//bind
	res = bind(sock, (struct sockaddr *) &local_addr, sizeof(local_addr));
	if(res < 0){
		fprintf(stderr, "Binding failed! (%s)\n", strerror(errno));
		return -2;
	}
	
	//setup listen
	res = listen(sock, 1);
	if(res < 0){
		fprintf(stderr, "Listen failed! (%s)\n", strerror(errno));
		return -3;
	}
		
	*out_listening_sock = sock;
	return 0;
}

int accept_TCP_client(int listening_sock, 
		    int * accepted_client_sock,
		    struct sockaddr_in * accepted_client_addr){
						
	unsigned int addr_size_l = sizeof(accepted_client_addr);
	*accepted_client_sock = accept(listening_sock,
				  (struct sockaddr *) accepted_client_addr, 
				   &addr_size_l);
				   
	if(*accepted_client_sock < 0){
		fprintf(stderr, "Accept failed, (%s)\n", strerror(errno));
		return -1;
	}
		
	return 0;
	
}

int close_socket(int sock){
	int res = close(sock);
	if(res < 0){
		fprintf(stderr, "Closing socket caused error! (%s)\n", strerror(errno));
		return -1;
	}
	return 0;
}

int set_receive_timeout(int sock, int timeout_in_seconds){
	struct timeval timeout;
	timeout.tv_sec = timeout_in_seconds;
	timeout.tv_usec = 0;
	int res = setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
	if(res < 0){
		fprintf(stderr, "Setting socket receive timeout failed! (%s)\n", strerror(errno));
		return -1;
	}
	return 0;
}

int receive_buffer(int sock, struct buffer * network_buffer){
    int res = recv(sock, network_buffer->memory, network_buffer->mem_size, 0);

    if(res < 0){
        fprintf(stderr, "Error while receiving! (%s)\n", strerror(errno));
        return -1;
    }

	network_buffer->dat_size = res;

    return 0;
}

int receive_buffer_nonblock(int sock, struct buffer * network_buffer){
    int res = recv(sock, network_buffer->memory, network_buffer->mem_size, MSG_DONTWAIT);

    if(res < 0){
		if(errno != EAGAIN){
        	fprintf(stderr, "Error while receiving! (%s)\n", strerror(errno));
			return -2;
		}
		return -1;
    }

	network_buffer->dat_size = res;

    return 0;
}

int send_buffer(int sock, struct buffer * network_buffer){
	int res = send(sock, network_buffer->memory, network_buffer->dat_size, 0);

	if(res < 0){
        //fprintf(stderr, "Error while sending! (%s)\n", strerror(errno));
		fprintf(stdout, "%s\n", strerror(errno));
        return -1;
    }

	reset_buffer(network_buffer);
	return res;
}

int receive_message(int client_sock, struct buffer * network_buffer, struct message * received_message){
    int result;
    reset_message(received_message);

    while(received_message->complete != true){
        if(network_buffer->dat_size == (network_buffer->last_processed + 1)){
            //all buffer data processed, ready to receive next buffer
            reset_buffer(network_buffer);
            result = receive_buffer(client_sock, network_buffer);
            if(result < 0) return result;
        }

        add_to_message(received_message, network_buffer);
    }
    return 0;
}

int receive_partial_message_nonblock(int client_sock, struct buffer * network_buffer, struct message * received_message){
	int result;

	if(network_buffer->dat_size == (network_buffer->last_processed + 1)){
        //all buffer data processed, ready to receive next buffer
        reset_buffer(network_buffer);
        result = receive_buffer_nonblock(client_sock, network_buffer);
        if(result < 0) return result;
    }

//	printf("adding to message\n");
    add_to_message(received_message, network_buffer);
	return 0;
}

char * address_to_string(struct sockaddr_in * address){
    return inet_ntoa(address->sin_addr);
}
