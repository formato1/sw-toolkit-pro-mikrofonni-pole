#pragma once
#include "utils.h"
#include "buffer.h"
#include "message.h"
#include <stdbool.h>
#include <arpa/inet.h>

/**
 * @brief Set-up server connection an act as server.
 * 
 * @param out_listening_sock listening sockets createt during procedure call
 * @param port port to be set up
 * @return error code
 */
int setup_TCP_server(int *out_listening_sock, int port);

/**
 * @brief Accepts client connection.
 * 
 * @param listening_sock listening socket
 * @param accepted_client_sock accepted client socket
 * @param accepted_client_addr address of accepted client
 * @return error code
 */
int accept_TCP_client(int listening_sock, 
		    int * accepted_client_sock,
		    struct sockaddr_in * accepted_client_addr);

/**
 * @brief Closes socket, frees allocated resources.
 * 
 * @param sock socket to be closed
 * @return error code
 */
int close_socket(int sock);

/**
 * @brief Sets receive timeou for a socket.
 *
 * @param sock socket to be set
 * @param timeout_in_seconds timeout in seconds
 * @return error code
 */
int set_receive_timeout(int sock, int timeout_in_seconds);

/**
 * @brief Receives buffer from network interface
 *
 * @param sock Accepted client socket.
 * @param network_buffer Buffer to store received data.
 * @return int. In case of failiure returns -1 and errno is set, otherwise returns 0. 
 */
int receive_buffer(int sock, struct buffer * network_buffer);
/**
 * @brief  * Sends a buffer. The buffer is reset after sending.

 * @param sock Accepted client socket.
 * @param network_buffer Buffer with data to send.
 * @return int. Number of sent bytes.
 */
int send_buffer(int sock, struct buffer * network_buffer);
/**
 * @brief Receives complete text message from network. To indicate end of received message is used newline separator.
 * @param client_sock Accepted client socket.
 * @param network_buffer Buffer for receiving network data.
 * @param received_message Output message.
 * @return int. Function uses receive_buffer(int, struct buffer). In case of failiure forwards return value of receive_buffer(int, struct buffer).
 */
int receive_message(int client_sock, struct buffer * network_buffer, struct message * received_message);
/**
 * @brief Receives text message from network. Message may be incomlpete. To indicate end of received message is used newline separator.
 * @param client_sock Accepted client socket.
 * @param network_buffer Buffer for receiving network data.
 * @param received_message Output message.
 * @return int. Function uses receive_buffer(int, struct buffer). In case of failiure forwards return value of receive_buffer(int, struct buffer).
 */
int receive_partial_message_nonblock(int client_sock, struct buffer * network_buffer, struct message * received_message);
char * address_to_string(struct sockaddr_in * address);