#from fileinput import filename
import socket
import time
import wave
from Settings import *


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    print("\n\nTDM client\n\n")
    print("press enter to start recording\n")
    input()

    s.settimeout(10)
    s.connect((HOST, PORT))
    configmsg = f"{frame_rate} {buffer_size} {device_name} {channels} {file_name}\n"
    s.sendall(bytes(configmsg, "ASCII"))
    s.sendall(b"record\n")

    print("recording started - press enter to stop recording")
    input()
    print("\nrecording stopped")
    print("receiving data")

    s.sendall(b"stop\n")
    s.sendall(bytes(f"fetch\n{file_name}\n", "ASCII"))

    file = open(file_name, "wb")
    
    while True:
        try:
            result = s.recv(1024)
            file.write(result)
        except:
            break
        if not result:
            break
       # print(result)

    file.close()
    s.sendall(b"disconnect\n")
    
    print("disconnected")
    print("processing received data")

fileobjs = []

for i in range(channels):
    fileobjs.append(wave.open("{0}/{1}.wav".format(output_folder,i),'wb'))
    fileobjs[i].setnchannels(1)
    fileobjs[i].setsampwidth(4)
    fileobjs[i].setframerate(frame_rate)

rawdatafile = open(file_name,"rb")

wavindex = 0
toskip = 0

if toskip != 0:
    rawdatafile.read(toskip)

while True:
    rawdata = rawdatafile.read(4)
    if(len(rawdata) == 0):
        break
    if(wavindex > (channels-1)): wavindex = 0
    fileobjs[wavindex].writeframesraw(rawdata)
    wavindex += 1

for f in fileobjs:
    f.close()


print("done")


