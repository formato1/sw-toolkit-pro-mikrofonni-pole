import socket

from Settings import AudioSettings
from Settings import NetworkSettings
from ChainSegment import ChainSegment
import threading
import time

#receives data and converts to numbers
class TCPClient(ChainSegment):
    def __init__(self, nextSegment:ChainSegment = None) -> None:
        super().__init__(nextSegment)
        self._port = NetworkSettings.port
        self._serverAddress = NetworkSettings.server_address
        self._sampleWidth = AudioSettings.sample_width
        self._numberOfReceivedBytes = 0
        self._close = False

    def passData(self, data) -> None:
        raise Exception("TCPClient is a data source!")

    def _start(self) -> None: 
        #socket init
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self._serverAddress, self._port))

        #send config message
        #sock.sendall(b"24000 500 hw:2 16 IMMEDIATE\n")
        config_message = f"{AudioSettings.sample_rate} {AudioSettings.buffer_size} {AudioSettings.ALSA_device_name} {AudioSettings.channels} IMMEDIATE\n"
        sock.sendall(bytes(config_message, "ASCII"))

        #start recording
        sock.sendall(b"record\n")    

        #receiving loop
        while True:
            rb = 0
            rawdata = []
            while(rb != NetworkSettings.buffer_size):
                #receives 16 integers of size 4
                rawdata.extend(sock.recv(NetworkSettings.buffer_size - rb))

                #no data, try again
                if rawdata == b'':
                    continue

                rb = len(rawdata)

            self._numberOfReceivedBytes += len(rawdata)
            #print('bytes received: {0}'.format(self._numberOfReceivedBytes), end='\r')

            #converts received byte array to little endian integers
            data = self.byteArrayToIntegerArray('little', rawdata)

            # passes data to next segment
            self.nextSegment.passData(data)

            if(self._close): break

        print("closing connection")
        sock.sendall(b"stop\n")
        sock.sendall(b"disconnect\n")
        time.sleep(1)
        
       # sock.close()

    def sendCTRL(self, sock, message):
        msg = bytearray(message, 'ASCII')
        msg.append('\n') #end of file
        sock.sendall(msg)


    def byteArrayToIntegerArray(self, endianity, rawdata):
            data = []
            start = 0
            end = self._sampleWidth
            for i in range(int(len(rawdata)/self._sampleWidth)):
                data.append(int.from_bytes(rawdata[start:end], endianity, signed=True))
                start += self._sampleWidth
                end += self._sampleWidth
            return data


    def startReceiving(self) -> None:
        self._windowThread = threading.Thread(target=self._start, daemon=True)
        self._windowThread.start()

    def stopReceiving(self):
        self._close = True
        self._windowThread.join()

