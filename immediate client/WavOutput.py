import wave
from Settings import AudioSettings
from ChainSegment import ChainSegment

#writes data in WAV format
class WavOutput(ChainSegment):
    def __init__(self,  nextSegment=None) -> None:
        super().__init__(nextSegment=nextSegment)
        self._fileobjs = []
        self._numberOfChannels = AudioSettings.channels

    def start(self, filename):
        for i in range(self._numberOfChannels):
            self._fileobjs.append(wave.open("{1}{0}.wav".format(i, filename),'wb'))
            self._fileobjs[i].setnchannels(1)
            self._fileobjs[i].setsampwidth(AudioSettings.sample_width)
            self._fileobjs[i].setframerate(AudioSettings.sample_rate)

    def passData(self, data) -> None:
        for i in range(self._numberOfChannels):
            self._fileobjs[i].writeframesraw(bytes(self._integerDataToByteArray(data[i])))

    def end(self):
        for f in self._fileobjs:
            f.close()

    def _integerDataToByteArray(self, data):
        result = []
        for d in data:
            result.extend(d.to_bytes(AudioSettings.sample_width, 'little', signed=True))
        return result
