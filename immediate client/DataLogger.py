from ChainSegment import ChainSegment

#logs data to a human readable file
class DataLogger(ChainSegment):
    def __init__(self, file, channel=None, nextSegment = None) -> None:
        super().__init__(nextSegment)
        self._file = file
        self._channel = channel

    def passData(self, data):
        pd = None

        if self._channel != None:
            pd = data[self._channel]
        else:
            pd = data

        print(pd, file= self._file)

        if self.nextSegment != None:
            self.nextSegment.passData(data)
