#interface for filters
class ChainSegment:
    def __init__(self, nextSegment = None) -> None:
        self.nextSegment = nextSegment
        pass
    #does the processing and passes data to the next segment
    def passData(self, data) -> None:
        raise NotImplementedError('passData must be implemented in subclass')