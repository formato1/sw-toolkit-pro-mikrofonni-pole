from TCPClient import TCPClient
from TDMChannelSplit import TDMChannelSplit
from WavOutput import WavOutput
from Settings import AudioSettings

#Building the signal chain
    #start from sink
    #make a instance of a chain segment and pass it as a constructor parameter of the next one.
    #chain segment data flow is TCPClient->TDMChannelSplit->WavOutput

writer = WavOutput() 
channelSplit = TDMChannelSplit(writer)
receiver = TCPClient(channelSplit)

#end of building a signal chain

print("\n\nTDM client\n\n")
print("press enter to start recording\n")
input()

#beginning of session
    #start modules that are needed to be started in order to work
    #writer.start creates output files
    #receiver.startReceiving() starts the pump element in the chain segment
#writer.start("outputs/test")
writer.start(AudioSettings.file_name)
receiver.startReceiving()

print("recording started - press enter to stop recording")
input()
print("\nrecording stopped")

#stops the session
receiver.stopReceiving()
writer.end()





