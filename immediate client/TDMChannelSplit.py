from ChainSegment import ChainSegment
from Settings import AudioSettings

#splits data to channels
class TDMChannelSplit(ChainSegment):
    def __init__(self, nextSegment=None) -> None:
        super().__init__(nextSegment=nextSegment)
        self._nuberOfChannels = AudioSettings.channels

    def passData(self, data) -> None:
        channelData = []
        currenChannel = 0

        #create array for every channel
        for c in range(self._nuberOfChannels):
            channelData.append([])

        #split sample by sample
        for d in data:
            channelData[currenChannel].append(d)
            currenChannel += 1
            if currenChannel == self._nuberOfChannels:
                currenChannel = 0
        
        self.nextSegment.passData(channelData)


