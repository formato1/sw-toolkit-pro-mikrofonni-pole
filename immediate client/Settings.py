class AudioSettings:
    sample_rate = 24000
    channels = 16
    sample_width = 4
    buffer_size = 500
    ALSA_device_name = "hw:2"
    file_name = "outputs/test"

class NetworkSettings:
    port = 5555
    server_address = "192.168.1.135"
    buffer_size = AudioSettings.channels * AudioSettings.sample_width * 128